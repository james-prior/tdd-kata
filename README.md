TDD Kata
========

This is a stub from which to learn or practice
[TDD](https://en.wikipedia.org/wiki/Test-driven_development).

[fizzbuzz](https://en.wikipedia.org/wiki/Fizz_buzz)
is good for learning or practicing TDD with,
because it has just enough complexity to not be completely trivial,
allowing one to focus on TDD and not so much on the problem.

In others words, fizzbuzz is good
[kata](https://en.wikipedia.org/wiki/Kata#Outside_martial_arts) for TDD.

Installation
------------

    git clone https://gitlab.com/james-prior/tdd-kata.git
    cd tdd-kata
    pipenv sync --dev

Do
--

Execute following in same directory as this `README.md` file.

    pipenv run pytest --color=yes -f .

In other windows, edit `fizzbuzz.py` and `tests/test_fizzbuzz.py`.

Notes
-----

The virtual environment was created with the following command:

    pipenv --python 3.9 install --dev pytest-xdist
