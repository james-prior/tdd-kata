from fizzbuzz import fizzbuzz
'''
examples of input and output
1 1
2 2
3 fizz
4 4
5 buzz
6 fizz
8 8
9 fizz
10 buzz
11 11
12 fizz
15 fizzbuzz
30 fizzbuzz
'''

def test_fizzbuzz_returns_1_given_1():
    expected = 1
    actual = fizzbuzz(1)
    assert actual == expected

# It is a common convention to put the expected value
# before the actual value in the assert statement,
# which is awkward. It might be necessary for junit,
# but we are using Python, so let's do the intuitive
# thing which is more readable:
# put the actual value before the expected value
#
#     assert actual == expected
